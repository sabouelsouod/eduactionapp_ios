//
//  TranscriptTableCell.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/16/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit

class TranscriptTableCell: UITableViewCell {
    @IBOutlet weak var subjectName: UILabel!

    @IBOutlet weak var subjectCode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
