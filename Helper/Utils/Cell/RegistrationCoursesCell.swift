//
//  GradeTableViewCell.swift
//  EducationApp
//
//  Created by eslammohamed on 11/16/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class RegistrationCoursesCell: UITableViewCell {

    @IBOutlet weak var courseCode: UILabel!
    @IBOutlet weak var courseName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
