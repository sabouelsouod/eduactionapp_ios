//
//  CoollectionCellCollectionViewCell.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/5/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class CollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var collectionImage: UIImageView!
    
    @IBOutlet weak var collectionName: UILabel!

}
