//
//  UserDataTableCell.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/12/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class UserDataTableCell: UITableViewCell {
    @IBOutlet weak var staticNames: UILabel!
    @IBOutlet weak var serverNames: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
staticNames.sizeToFit()
    serverNames.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
