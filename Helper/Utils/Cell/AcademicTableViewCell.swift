
//
//  AcademicTableViewCell.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/12/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class AcademicTableViewCell: UITableViewCell {

    @IBOutlet weak var staticName: UILabel!
    @IBOutlet weak var serverName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
