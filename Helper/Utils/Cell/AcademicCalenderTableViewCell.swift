//
//  AcademicTableViewCell.swift
//  EducationApp
//
//  Created by eslammohamed on 11/21/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class AcademicCalenderTableViewCell: UITableViewCell {
    @IBOutlet weak var collageName: UILabel!

    @IBOutlet weak var semesterName: UILabel!
    
    @IBOutlet weak var activityName: UILabel!
    
    @IBOutlet weak var toDate: UILabel!
    @IBOutlet weak var fromDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
