//
//  SideTableCell.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/5/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class SideTableCell: UITableViewCell {

    @IBOutlet weak var tableImage: UIImageView!
    @IBOutlet weak var tableName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
