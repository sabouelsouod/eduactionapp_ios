//
//  Updatable.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/13/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
protocol Updatable {
    func update()
    func update(APiName:String)
    func updateRefresh()
    
}
