//
//  SearchSemesterAcademicModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/20/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class SearchSemesterAcademicModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }
    
     var eD_STUD_SEMESTER_ID : String?
     var sEMESTER_DESCR_EN : String?
     var sEMESTER_DESCR_AR : String?
     var transcripts : String?

    
    func mapping(map: Map) {
        
        eD_STUD_SEMESTER_ID<-map["ED_STUD_SEMESTER_ID"]
        sEMESTER_DESCR_EN<-map["SEMESTER_DESCR_EN"]
        sEMESTER_DESCR_AR<-map["SEMESTER_DESCR_AR"]
        transcripts<-map["Transcripts"]
    }
    
    
}
