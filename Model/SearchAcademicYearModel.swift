//
//  SearchAcademicYearModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/20/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class SearchAcademicYearModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }
    
     var eD_ACAD_YEAR_ID : String?
     var aCAD_YEAR_DESCR_AR : String?
     var aCAD_YEAR_DESCR_EN : String?
     var semesters : String?
    
    func mapping(map: Map) {
        eD_ACAD_YEAR_ID<-map["ED_ACAD_YEAR_ID"]
        aCAD_YEAR_DESCR_AR<-map["ACAD_YEAR_DESCR_AR"]
        aCAD_YEAR_DESCR_EN<-map["ACAD_YEAR_DESCR_EN"]
        semesters<-map["Semesters"]
    }

        
}
