//
//  UserDataModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/13/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class UserDataModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return 
    }

    
    var studentID: String?
    var faculty_ID: String?
    var semesterCode: String?
    var academicYear:String?
    var yearDescrip_EN: String?
    var yearDescrip_AR: String?
    var yearCode: String?
    var semsDescrip_EN: String?
    var semsDescrip_AR: String?
    
    func mapping(map: Map) {
        
        studentID <- map["ED_STUD_ID"]
        faculty_ID <- map["AS_FACULTY_INFO_ID"]
        semesterCode <- map["ED_CODE_SEMESTER_ID"]
        academicYear <- map["ED_ACAD_YEAR_ID"]
        yearDescrip_EN <- map["YEAR_DESC_EN"]
        yearDescrip_AR <- map["YEAR_DESC_AR"]
        yearCode <- map["YEAR_CODE"]
        semsDescrip_EN <- map["SEM_DESC_EN"]
        semsDescrip_AR <- map["SEM_DESC_AR"]
        
         }
    
}
