//
//  SemesterModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/15/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class SemesterModel: Mappable {
    public required init?(map: Map) {
        return
    }
    var ED_STUD_SEMESTER_ID: String?
    var SEMESTER_DESCR_EN: String?
    var SEMESTER_DESCR_AR: String?
    var Transcripts = [TranscriptModel]()
    func mapping(map: Map) {
        
        ED_STUD_SEMESTER_ID<-map["ED_STUD_SEMESTER_ID"]
        SEMESTER_DESCR_EN<-map["SEMESTER_DESCR_EN"]
        SEMESTER_DESCR_AR<-map["SEMESTER_DESCR_AR"]
        Transcripts<-map["Transcripts"]
        
        
    }
    
}
