//
//  CourseCatalogModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/26/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class CourseCatalogModel: Mappable {
    public required init?(map: Map) {
        return
    }
     var aS_FACULTY_INFO_ID : String?
     var fACULTY_DESCR_AR : String?
     var fACULTY_DESCR_EN : String?
     var cOURSE_CODE : String?
     var cOURSE_DESCR_AR : String?
     var cOURSE_DESCR_EN : String?
     var cREDIT_HOURS : String?
     var cOURSE_CONTENTS_AR : String?
     var expand:Bool = false
    
    
    func mapping(map: Map) {
        aS_FACULTY_INFO_ID<-map["AS_FACULTY_INFO_ID"]
        fACULTY_DESCR_AR<-map["FACULTY_DESCR_AR"]
        fACULTY_DESCR_EN<-map["FACULTY_DESCR_EN"]
        cOURSE_CODE<-map["COURSE_CODE"]
        cOURSE_DESCR_AR<-map["COURSE_DESCR_AR"]
        cOURSE_DESCR_EN<-map["COURSE_DESCR_EN"]
        cREDIT_HOURS<-map["CREDIT_HOURS"]
        cOURSE_CONTENTS_AR<-map["COURSE_CONTENTS_AR"]
        
    }
    
    
}
