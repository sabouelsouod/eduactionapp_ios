//
//  AcademicCalenderModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/21/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class AcademicCalenderModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }
    
//    public var eD_ACAD_YEAR_ID : Int?
//    public var eD_CODE_SEMESTER_ID : Int?
//    public var yEAR_CALENDER : Int?
    public var sEM_AR : String?
    public var sEM_EN : String?
    public var eNT_DESCR_AR : String?
    public var eNT_DESCR_EN : String?
    public var aCTV_AR : String?
    public var aCTV_EN : String?
    public var fROM_DATE : String?
    public var tO_DATE : String?
//    public var oNLINE_FLG : Bool?
    
    func mapping(map: Map) {
        
//        eD_ACAD_YEAR_ID<-map["ED_ACAD_YEAR_ID"]
//        eD_CODE_SEMESTER_ID<-map["ED_CODE_SEMESTER_ID"]
//        yEAR_CALENDER<-map["YEAR_CALENDER"]
        sEM_AR<-map["SEM_AR"]
        sEM_EN<-map["SEM_EN"]
        eNT_DESCR_AR<-map["ENT_DESCR_AR"]
        eNT_DESCR_EN<-map["ENT_DESCR_EN"]
        aCTV_AR<-map["ACTV_AR"]
        aCTV_EN<-map["ACTV_EN"]
        fROM_DATE<-map["FROM_DATE"]
        tO_DATE<-map["TO_DATE"]
//        oNLINE_FLG<-map["ONLINE_FLG"]
    }
    
    
}
