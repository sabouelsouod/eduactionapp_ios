//
//  CatalogFacultiesModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/22/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class CatalogFacultiesModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }
    
    public var id : Int?
    public var nameAr : String?
    public var nameEn : String?
    public var type : Int?
    public var asFacultyInfoId : Int?
    var department = [CatalogDepartmentModel]()
    public var majors : String?

    
    
    func mapping(map: Map) {
        
        id<-map["Id"]
        nameAr<-map["NameAr"]
        nameEn<-map["NameEn"]
        type<-map["Type"]
        asFacultyInfoId<-map["AsFacultyInfoId"]
        majors<-map["Majors"]
        department<-map["Departments"]


    }
}
