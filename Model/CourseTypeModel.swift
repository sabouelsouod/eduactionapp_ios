//
//  CourseTypeModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/22/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class CourseTypeModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }

public var eD_CODE_COURSE_ID : Int?
public var dESCR_AR : String?
public var dESCR_EN : String?
    
    
    func mapping(map: Map) {
        
        eD_CODE_COURSE_ID<-map["ED_CODE_COURSE_ID"]
        dESCR_AR<-map["DESCR_AR"]
        dESCR_EN<-map["DESCR_EN"]
    }
  
    
    
    
    
    
    
    
    
    
    
    
}
