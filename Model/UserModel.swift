//
//  UserLogin.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/7/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

//
//  UserLogin.swift
//  EducationAPP
//
//  Created by eslammohamed on 11/7/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper
class UserModel: Mappable{
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return 
    }

    
    var studentName_EN: String?
    var studentCode: String?
    var studentName_AR: String?
    var nationality_EN: String?
    var nationality_AR: String?
    var nationalID_EN: String?
    var email: String?
    var major_EN: String?
    var major_AR: String?
    var phoneNo: String?
    var faculty_EN: String?
    var faculty_AR: String?
    var degree_EN: String?
    var degree_AR: String?
    var level_AR: String?
    var level_EN: String?
    var semesterCH: String?
    var semesterGpa: String?
    var acumlateGpa: String?
    var acumlatePoint: String?
    var acumlateCredit: String?
    var degreeGranted: String?
    var enroll_EN: String?
    var enroll_AR: String?
    var x  = [String]()
    
    func mapping(map: Map) {
        
        
        studentName_EN <- map["FULL_NAME_EN"]
        studentName_AR <- map["FULL_NAME_AR"]
        studentCode <- map["STUD_FACULTY_CODE"]
        nationality_EN <- map["NATION_DESCR_EN"]
        nationality_AR <- map["NATION_DESCR_AR"]
        nationalID_EN <- map["NATIONAL_NUMBER"]
        email <- map["STUD_EMAIL"]
        major_EN <- map["MAJOR_EN"]
        major_AR <- map["MAJOR_AR"]
        phoneNo <- map["STUD_MOBNO"]
        faculty_EN <- map["FACULTY_DESCR_EN"]
        faculty_AR <- map["FACULTY_DESCR_AR"]
        degree_EN <- map["DEGREE_EN"]
        degree_AR <- map["DEGREE_AR"]
        level_AR <- map["LEVEL_AR"]
        level_EN <- map["LEVEL_EN"]
        semesterCH <- map["SEM_CH"]
        semesterGpa <- map["SEM_GPA"]
        acumlateGpa <- map["ACCUM_GPA"]
        acumlatePoint <- map["ACCUM_POINT"]
        acumlateCredit <- map["ACCUM_CH"]
        degreeGranted <- map["GRADUATES_FLAG"]
        enroll_EN <- map["ENROLL_EN"]
        enroll_AR <- map["ENROLL_AR"]
        
        
    }
    func userDataArry() -> (Array<Any>,Array<Any>) {
        let userData_EN = [studentCode ,studentName_EN,nationality_EN, nationalID_EN, email,phoneNo]
        
        let userData_AR = [studentCode ,studentName_AR, nationality_AR, nationalID_EN, email, phoneNo]
        
        return (userData_EN,userData_AR)
    }
    func userProfileArry() -> (Array<Any>, Array<Any>) {
        let userProfile_EN = [ faculty_EN,degree_EN,major_EN, level_EN, semesterCH,semesterGpa,acumlateGpa, acumlatePoint, degreeGranted, enroll_EN]
        let userProfile_AR = [faculty_AR, degree_AR, major_AR, level_AR, semesterCH,semesterGpa,acumlateGpa, acumlatePoint, degreeGranted,enroll_AR]

        
        
        return (userProfile_EN , userProfile_AR)
    }
    
}
