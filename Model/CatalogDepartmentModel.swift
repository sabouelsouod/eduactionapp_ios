//
//  CatalogDepartmentModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/22/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class CatalogDepartmentModel: Mappable {
    public required init?(map: Map) {
        return
    }
    public var id : Int?
    public var nameAr : String?
    public var nameEn : String?
    public var type : Int?
    public var asFacultyInfoId : Int?
    public var departments : String?
    public var majors : String?
    
    
    func mapping(map: Map) {
        
        id<-map["Id"]
        nameAr<-map["NameAr"]
        nameEn<-map["NameEn"]
        type<-map["Type"]
        asFacultyInfoId<-map["AsFacultyInfoId"]
        departments<-map["Departments"]
        majors<-map["Majors"]
        
    }
    
}
