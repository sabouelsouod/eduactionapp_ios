//
//  StudentScheduleModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class YearSemestersModel: Mappable {
    public required init?(map: Map) {
        return
    }
    var ED_ACAD_YEAR_ID: String?
    var ACAD_YEAR_DESCR_AR: String?
    var ACAD_YEAR_DESCR_EN: String?
    var Semesters = [SemesterModel]()
    
    
    func mapping(map: Map) {
        
         ED_ACAD_YEAR_ID<-map["ED_ACAD_YEAR_ID"]
         ACAD_YEAR_DESCR_AR<-map["ACAD_YEAR_DESCR_AR"]
         ACAD_YEAR_DESCR_EN<-map["ACAD_YEAR_DESCR_EN"]
        Semesters<-map["Semesters"]
        
    }
    
}
