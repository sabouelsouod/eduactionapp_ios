//
//  TranscriptModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/15/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class TranscriptModel: Mappable {
    public required init?(map: Map) {
        return
    }
//     var eD_CODE_SEMESTER_ID : String?
//     var eD_STUD_SEMESTER_ID : String?
//     var eD_ACAD_YEAR_ID : String?
     var sEMESTER_DESCR_AR : String?
     var sEMESTER_DESCR_EN : String?
     var aCAD_YEAR_DESCR_AR : String?
     var aCAD_YEAR_DESCR_EN : String?
     var sTUD_FACULTY_CODE : String?
     var eD_STUD_ID : String?
     var cOURSE_CODE : String?
//     var pF_FLG : String?
     var cOURSE_DESCR_AR : String?
     var cOURSE_DESCR_EN : String?
     var cREDIT_HOURS : String?
//     var rEPEAT_FLG : String?
//     var rEPEAT_NO : String?
//     var sTATUS_DESCR_AR : String?
//     var sTATUS_DESCR_EN : String?
//     var sYMBOL : String?
     var gRADING_AR : String?
     var gRADING_EN : String?
     var cOURSE_DEGREE : String?
     var cOURSE_POINT : String?
     var symbol_EN : String?
     var symbol_AR : String?
//     var aCCUM_CH : String?
     var aCCUM_GPA : String?
     var aCCUM_POINT : String?
     var aCCUM_CH_TOT : String?
  //   var aCCUM_POINT_TOT : String?
     var sEM_CH : String?
     var sEM_GPA : String?
     var sEM_POINT : String?
     var mAJOR_CGPA : String?
     var enroll_Date : String?
//     var sTATES_AR : String?
//     var sTATES_EN : String?
//     var stf_name_ar : String?
//     var stf_name_en : String?
//     var fULL_NAME_EN : String?
//     var fULL_NAME_AR : String?
//     var yEAR_ORDER : String?
//     var sEMESTER_ORDER : String?
//     var gS_CODE_PASS_FAIL_ID : String?
//     var adm_year_ar : String?
//     var aBS_FLG : String?
//     var adm_year_en : String?
//     var adm_sem_en : String?
//     var adm_sem_ar : String?
//     var cRS_NO : String?
//     var iS_RESULT_APPROVED : String?
//     var aDD_GPA_FLG : String?
//     var eD_CODE_COURSE_STATUS_ID : String?
//     var aCAD_WARN_TYPE_DESCR_EN : String?
//     var aCAD_WARN_TYPE_DESCR_AR : String?
     var eNROLL_AR : String?
     var eNROLL_EN : String?
//     var eD_CODE_ENROLL_TYPE_ID : String?
//     var regSemCH : String?
//     var regAccumCH : String?
//     var hOLD_FOR_GRAD_FLG : String?
    
    
    
    
    func mapping(map: Map) {
        
//        eD_CODE_SEMESTER_ID<-map["ED_CODE_SEMESTER_ID"]
//        eD_STUD_SEMESTER_ID<-map["ED_STUD_SEMESTER_ID"]
//        eD_ACAD_YEAR_ID<-map["ED_ACAD_YEAR_ID"]
        sEMESTER_DESCR_AR<-map["SEMESTER_DESCR_AR"]
        sEMESTER_DESCR_EN<-map["SEMESTER_DESCR_EN"]
        aCAD_YEAR_DESCR_AR<-map["ACAD_YEAR_DESCR_AR"]
        aCAD_YEAR_DESCR_EN<-map["ACAD_YEAR_DESCR_EN"]
        sTUD_FACULTY_CODE<-map["STUD_FACULTY_CODE"]
        eD_STUD_ID<-map["ED_STUD_ID"]
        cOURSE_CODE<-map["COURSE_CODE"]
        cOURSE_DESCR_AR<-map["COURSE_DESCR_AR"]
        cOURSE_DESCR_EN<-map["COURSE_DESCR_EN"]
//        pF_FLG<-map["PF_FLG"]
        cREDIT_HOURS<-map["CREDIT_HOURS"]
//        aBS_FLG<-map["ABS_FLG"]
//        rEPEAT_FLG<-map["REPEAT_FLG"]
//        rEPEAT_NO<-map["REPEAT_NO"]
//        sTATUS_DESCR_AR<-map["STATUS_DESCR_AR"]
//        sTATUS_DESCR_EN<-map["STATUS_DESCR_EN"]
//        sYMBOL<-map["SYMBOL"]
        gRADING_AR<-map["GRADING_AR"]
        gRADING_EN<-map["GRADING_EN"]
        cOURSE_DEGREE<-map["COURSE_DEGREE"]
        cOURSE_POINT<-map["COURSE_POINT"]
        symbol_EN<-map["Symbol_EN"]
        symbol_AR<-map["Symbol_AR"]
//        aCCUM_CH<-map["ACCUM_CH"]
        aCCUM_GPA<-map["ACCUM_GPA"]
        aCCUM_POINT<-map["ACCUM_POINT"]
        aCCUM_CH_TOT<-map["ACCUM_CH_TOT"]
  //      aCCUM_POINT_TOT<-map["ACCUM_POINT_TOT"]
        sEM_CH<-map["SEM_CH"]
        sEM_GPA<-map["SEM_GPA"]
        sEM_POINT<-map["SEM_POINT"]
        mAJOR_CGPA<-map["MAJOR_CGPA"]
        enroll_Date<-map["Enroll_Date"]
//        sTATES_AR<-map["STATES_AR"]
//        sTATES_EN<-map["STATES_EN"]
//        stf_name_ar<-map["Stf_name_ar"]
//        stf_name_en<-map["Stf_name_en"]
//        fULL_NAME_EN<-map["FULL_NAME_EN"]
//        fULL_NAME_AR<-map["FULL_NAME_AR"]
//        yEAR_ORDER<-map["YEAR_ORDER"]
//        sEMESTER_ORDER<-map["SEMESTER_ORDER"]
//        gS_CODE_PASS_FAIL_ID<-map["GS_CODE_PASS_FAIL_ID"]
//        adm_year_ar<-map["adm_year_ar"]
//        adm_year_en<-map["adm_year_en"]
//        adm_sem_en<-map["adm_sem_en"]
//        adm_sem_ar<-map["adm_sem_ar"]
//        cRS_NO<-map["CRS_NO"]
//        iS_RESULT_APPROVED<-map["IS_RESULT_APPROVED"]
//        aDD_GPA_FLG<-map["ADD_GPA_FLG"]
//        eD_CODE_COURSE_STATUS_ID<-map["ED_CODE_COURSE_STATUS_ID"]
//        aCAD_WARN_TYPE_DESCR_EN<-map["ACAD_WARN_TYPE_DESCR_EN"]
//        aCAD_WARN_TYPE_DESCR_AR<-map["ACAD_WARN_TYPE_DESCR_AR"]
        eNROLL_AR<-map["ENROLL_AR"]
        eNROLL_EN<-map["ENROLL_EN"]
//        eD_CODE_ENROLL_TYPE_ID<-map["ED_CODE_ENROLL_TYPE_ID"]
//        regSemCH<-map["RegSemCH"]
//        regAccumCH<-map["RegAccumCH"]
//        hOLD_FOR_GRAD_FLG<-map["HOLD_FOR_GRAD_FLG"]

        
        
    }
    func semesterGarde() -> (Array<Any>,Array<Any>) {
        let semesterData_EN = [eNROLL_EN,sEM_GPA,sEM_CH,sEM_POINT,aCCUM_GPA,aCCUM_CH_TOT,aCCUM_POINT,mAJOR_CGPA]
        let semesterData_AR = [eNROLL_AR,sEM_GPA,sEM_CH,sEM_POINT,aCCUM_GPA,aCCUM_CH_TOT,aCCUM_POINT,mAJOR_CGPA]

        
        
        return (semesterData_EN,semesterData_AR)
    }
    func subjectGrade() -> (Array<Any>,Array<Any>){
        let subjectData_EN = [symbol_EN,cOURSE_DEGREE,cREDIT_HOURS,cOURSE_POINT,enroll_Date]
        let subjectData_AR = [symbol_AR,cOURSE_DEGREE,cREDIT_HOURS,cOURSE_POINT,enroll_Date]
        
        
        
        return (subjectData_EN,subjectData_AR)

    }
    
}
