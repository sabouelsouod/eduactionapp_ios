//
//  CourseRegisterModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/19/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class CourseRegisterModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
}
//     var cRS_SEC_ORDR : String?
//     var eD_STUD_COURSE_REG_ID : String?
     var cOURSE_CODE : String?
     var cOURSE_DESCR_AR : String?
     var cOURSE_DESCR_EN : String?
//     var sC_SCHEDULE_DTL_ID : String?
//     var eD_COURSE_ID : String?
     var cREDIT_HOURS : String?
//     var rEPEAT_NO : String?
//     var pAY_FLG : String?
//     var pAY_AMOUNT : String?
//     var nOTES : String?
//     var wAIT_NO : String?
//     var eD_CODE_COURSE_STATUS_ID : String?
//     var eD_CDE_TCHNG_MTHD_ID : String?
//     var dESCR_EN : String?
//     var dESCR_AR : String?
//     var eD_STUD_GROUP_ID : String?
     var sYMBOL : String?
//     var gROUP_ORDER : String?
//     var sE_USER_ID : String?
//     var gROUP_ORDER_DESCR : String?
//     var gROUP_DESCR_AR : String?
//     var gROUP_DESCR_EN : String?
//     var eD_STUD_SEMESTER_ID : String?
//     var eD_CODE_COURSE_ID : String?
//     var sTD_CRS_CH : String?

    
    func mapping(map: Map) {
//        cRS_SEC_ORDR<-map["CRS_SEC_ORDR"]
//        eD_STUD_COURSE_REG_ID<-map["ED_STUD_COURSE_REG_ID"]
        cOURSE_CODE<-map["COURSE_CODE"]
        cOURSE_DESCR_AR<-map["COURSE_DESCR_AR"]
        cOURSE_DESCR_EN<-map["COURSE_DESCR_EN"]
//        sC_SCHEDULE_DTL_ID<-map["SC_SCHEDULE_DTL_ID"]
//        eD_COURSE_ID<-map["ED_COURSE_ID"]
        cREDIT_HOURS<-map["CREDIT_HOURS"]
//        rEPEAT_NO<-map["REPEAT_NO"]
//        pAY_FLG<-map["PAY_FLG"]
//        pAY_AMOUNT<-map["PAY_AMOUNT"]
//        nOTES<-map["NOTES"]
//        wAIT_NO<-map["WAIT_NO"]
//        eD_CODE_COURSE_STATUS_ID<-map["ED_CODE_COURSE_STATUS_ID"]
//        eD_CDE_TCHNG_MTHD_ID<-map["ED_CDE_TCHNG_MTHD_ID"]
//        dESCR_EN<-map["DESCR_EN"]
//        dESCR_AR<-map["DESCR_AR"]
//        eD_STUD_GROUP_ID<-map["ED_STUD_GROUP_ID"]
        sYMBOL<-map["SYMBOL"]
//        gROUP_ORDER<-map["GROUP_ORDER"]
//        sE_USER_ID<-map["SE_USER_ID"]
//        gROUP_ORDER_DESCR<-map["GROUP_ORDER_DESCR"]
//        gROUP_DESCR_AR<-map["GROUP_DESCR_AR"]
//        gROUP_DESCR_EN<-map["GROUP_DESCR_EN"]
//        eD_STUD_SEMESTER_ID<-map["ED_STUD_SEMESTER_ID"]
//        eD_CODE_COURSE_ID<-map["ED_CODE_COURSE_ID"]
//        sTD_CRS_CH<-map["STD_CRS_CH"]

  
    }
    
    
    
    func coursesDataArry() -> (Array<Any>,Array<Any>) {
        let courseData_EN = [cOURSE_DESCR_EN ,cOURSE_CODE]
        
        let courseData_AR = [cOURSE_DESCR_AR ,cOURSE_CODE]
        
        return (courseData_EN,courseData_AR)
    }
    func coursesInfoArry() -> (Array<Any>,Array<Any>) {
        let courseInfo_EN = [cOURSE_CODE ,cOURSE_DESCR_EN,cREDIT_HOURS,sYMBOL]
        
        let courseInfo_AR = [cOURSE_CODE,cOURSE_DESCR_AR ,cREDIT_HOURS,sYMBOL]
        
        return (courseInfo_EN,courseInfo_AR)
    }
}



