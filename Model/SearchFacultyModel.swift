//
//  SearchFacultyModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/20/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper
class SearchFacultyModel: Mappable {
    /// This function can be used to validate JSON prior to mapping. Return nil to cancel mapping at this point
    public required init?(map: Map) {
        return
    }
    
     var id : Int?
     var nameAr : String?
     var nameEn : String?
     var type : String?
     var asFacultyInfoId : Int?
     var departments : String?
     var majors : String?
    
    
    
        func mapping(map: Map) {
            id<-map["Id"]
            nameAr<-map["NameAr"]
            nameEn<-map["NameEn"]
            type<-map["Type"]
            asFacultyInfoId<-map["AsFacultyInfoId"]
            departments<-map["Departments"]
            majors<-map["Majors"]

        
        }
}
