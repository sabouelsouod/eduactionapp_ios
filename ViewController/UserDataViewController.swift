//
//  UserDataViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class UserDataViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,ExpandableHeaderViewDelegate {
    let names = ["Student Code:","Student Name:","Nationality:","National ID:","E-Mail:","Mobile No:"]
    
    let acadmicName = ["College:","Degree:","Major:","Level:","Semester CH:","Full Feild CH:","CGPA:","GPA:","Degree Granted:","Enrollment Status:"]
    
    var expand:Bool = false
    @IBOutlet weak var academicTableView: UITableView!
    @IBOutlet weak var userDataTableView: UITableView!
    
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        academicTableView.delegate = self
        academicTableView.dataSource = self
        academicTableView.tag = 2
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        var contentRect = CGRect.zero
        for view: UIView in scrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
//        if(secondView.isHidden == true){
//        contentRect.size.height = contentRect.size.height - 250
//            
//        scrollView.contentSize = contentRect.size
//        }else{
//            contentRect.size.height = contentRect.size.height
//            scrollView.contentSize = contentRect.size
//        }
    }
   

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView.tag == 2){
            return acadmicName.count
        }
        else{
            
            return names.count
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView.tag == 2){
            return 44
        }
        else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView.tag == 2){
            if(expand == true){
                return 44
            }
            else{
                return 0
            }
        }
        else{
            return 44
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        
        header.customInit(arrow: ">",title: "Academic Data", section: section, delegate: self)
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView.tag == 2){
            let userProfileArry_EN = LoginViewModel.instance.user?.userProfileArry().0 as! [String]
            let userProfileArry_AR = LoginViewModel.instance.user?.userProfileArry().1 as! [String]

            let cell = academicTableView.dequeueReusableCell(withIdentifier: "academiccell", for: indexPath) as! AcademicTableViewCell
            cell.staticName.text = NSLocalizedString(acadmicName[indexPath.row], comment: "")
            
            if L102Language.currentAppleLanguage() == "en" {
                cell.serverName.text = userProfileArry_EN[indexPath.row]
            }
           else{
                cell.serverName.text = userProfileArry_AR[indexPath.row]
                
            }
            return cell
        }
        else{
            let cell = userDataTableView.dequeueReusableCell(withIdentifier: "dataCell", for: indexPath) as! UserDataTableCell
            let userDataArry_EN = LoginViewModel.instance.user?.userDataArry().0 as! [String]
            let userDataArry_AR = LoginViewModel.instance.user?.userDataArry().1 as! [String]


            cell.staticNames.text = NSLocalizedString(names[indexPath.row], comment: "")
            if L102Language.currentAppleLanguage() == "en" {
                cell.serverNames.text = userDataArry_EN[indexPath.row]            }
            else{
                cell.serverNames.text = userDataArry_AR[indexPath.row]
                
            }
            return cell
            
        }
    }
    
    func toggleSection(header: ExpandableHeaderView, section: Int) {
        expand = !expand
        
        
        academicTableView.beginUpdates()
        for i in 0 ..< acadmicName.count {
            academicTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
            header.setCollapsed(!expand)
        }
        academicTableView.endUpdates()
    }
    
        
        
        
    }

   


