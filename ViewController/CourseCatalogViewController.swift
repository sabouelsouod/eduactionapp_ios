
//
//  CourseCatalogViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/26/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
class CourseCatalogViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ExpandableHeaderViewDelegate {
    @IBOutlet weak var courseCatalogTableView: UITableView!
var sec = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return CourseCatalogViewModel.instance.data.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = ExpandableHeaderView()
        
        header.customInit(arrow:"+",title: CourseCatalogViewModel.instance.data[section].cOURSE_DESCR_EN!, section: section, delegate: self)
        return header
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = courseCatalogTableView.dequeueReusableCell(withIdentifier: "coursecatalog", for: indexPath) as! CourseCatalogTableCell
        cell.courseCode.text = "eslam"
        cell.courseDescription.text = CourseCatalogViewModel.instance.data[indexPath.section].cOURSE_CONTENTS_AR
        
        cell.courseCreditHour.text = CourseCatalogViewModel.instance.data[indexPath.section].cREDIT_HOURS
        cell.courseCode.text = CourseCatalogViewModel.instance.data[indexPath.section].cOURSE_CODE
        
        
        
        return cell
        }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
            return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if( CourseCatalogViewModel.instance.data[indexPath.section].expand == true ){
        return 371
        }
        else{
                return 0
        }
    }
    
    func toggleSection(header: ExpandableHeaderView, section: Int) {
       // expand = !expand
        CourseCatalogViewModel.instance.data[section].expand = !CourseCatalogViewModel.instance.data[section].expand
        courseCatalogTableView.beginUpdates()
        for i in 0 ..< 1 {
            courseCatalogTableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
            header.setCollapsed(!CourseCatalogViewModel.instance.data[section].expand)
        }
        courseCatalogTableView.endUpdates()
}
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }
        
}
