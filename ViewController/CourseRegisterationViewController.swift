//
//  CourseRegisterationViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/19/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CourseRegisterationViewController: UIViewController,Updatable,UITableViewDelegate,UITableViewDataSource ,NVActivityIndicatorViewable{
    @IBOutlet weak var registerTableView: UITableView!
    let size = CGSize(width: 30, height: 30)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startAnimating(self.size, message: "Loading...", type: .lineScalePulseOutRapid)

        RegistrationViewModel.instance.studentRecord(studentID: "35", academicYear: "7", semesterCode: "1", updatable: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func update(){
        self.stopAnimating()

        registerTableView.reloadData()
      
    }
    func update(APiName:String)
    {
    }
    
    
    
    func updateRefresh(){
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RegistrationViewModel.instance.record.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = registerTableView.dequeueReusableCell(withIdentifier: "registercourse", for: indexPath) as! RegistrationCoursesCell
        if L102Language.currentAppleLanguage() == "en" {
            cell.courseCode.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().0[1] as? String
            cell.courseName.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().0[0] as? String
        }
        else{
            cell.courseCode.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().1[1] as? String
            cell.courseName.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().1[0] as? String
        }
//        cell.courseCode.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().0[1] as! String
//        cell.courseName.text = RegistrationViewModel.instance.record[indexPath.row].coursesDataArry().0[0] as! String
       
          return cell
        }


}
