//
//  SearchCourseCatalogViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/22/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SearchCourseCatalogViewController: UIViewController,Updatable,UIPickerViewDelegate,UIPickerViewDataSource,BEMCheckBoxDelegate,NVActivityIndicatorViewable {
    @IBOutlet weak var courseTypeSelector: UITextField!
    var courseTypeSelected = 0
    @IBOutlet weak var collageSelector: UITextField!
    var collageSelected = 0
    var depcollageSelected = 0
    let size = CGSize(width: 30, height: 30)

    @IBOutlet weak var departmentSelector: UITextField!
    var departmentSelected = 0
    @IBOutlet weak var activeAllCheck: BEMCheckBox!
    @IBOutlet weak var activeYesCheck: BEMCheckBox!
    @IBOutlet weak var activeNoCheck: BEMCheckBox!
    @IBOutlet weak var onlineAllCheck: BEMCheckBox!
    @IBOutlet weak var onlineYesCheck: BEMCheckBox!
    @IBOutlet weak var onlineNoCheck: BEMCheckBox!
    @IBOutlet weak var degreeAllCheck: BEMCheckBox!
    @IBOutlet weak var underGradeCheck: BEMCheckBox!
    @IBOutlet weak var postGradeCheck: BEMCheckBox!
    
    @IBOutlet weak var CourseCodeTXT: UITextField!
    
    var selectedCollage:Int? = nil
    
    let url = Constants()
    let toolBar = UIToolbar()

    override func viewDidLoad() {
        super.viewDidLoad()

        SearchCourseCatalogViewModel.instance.searchAcademicData(updatable: self)
        createToolbar()
        createCoursePicker()
        createCollagePicker()
        createDepertmentPicker()
        activeAllCheck.delegate = self
        activeYesCheck.delegate = self
        activeNoCheck.delegate = self
        onlineAllCheck.delegate = self
        onlineYesCheck.delegate = self
        onlineNoCheck.delegate = self
        degreeAllCheck.delegate = self
        underGradeCheck.delegate = self
        postGradeCheck.delegate = self
        
    }
    func didTap(_ checkBox: BEMCheckBox) {
        print(checkBox.tag)
        if (checkBox.tag == 0 && degreeAllCheck.on == true){
            postGradeCheck.on = false
            underGradeCheck.on = false
        }
        else if(checkBox.tag == 1 && underGradeCheck.on == true){
            degreeAllCheck.on = false
            postGradeCheck.on = false
        }
        else if (checkBox.tag == 2 && postGradeCheck.on == true){
            degreeAllCheck.on = false
            underGradeCheck.on = false
        }
        
        else if (checkBox.tag == 3 && onlineAllCheck.on == true){
            onlineYesCheck.on = false
            onlineNoCheck.on = false
        }
        else if (checkBox.tag == 4 && onlineYesCheck.on == true){
            onlineAllCheck.on = false
            onlineNoCheck.on = false
        }
        else if (checkBox.tag == 5 && onlineNoCheck.on == true){
            onlineYesCheck.on = false
            onlineAllCheck.on = false
        }
        else if (checkBox.tag == 6 && activeAllCheck.on == true){
            activeNoCheck.on = false
            activeYesCheck.on = false
        }
        else if (checkBox.tag == 7 && activeYesCheck.on == true){
            activeAllCheck.on = false
            activeNoCheck.on = false
        }
        else if (checkBox.tag == 8 && activeNoCheck.on == true){
            activeAllCheck.on = false
            activeYesCheck.on = false
        }
        
        
        
        
    }
    func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        CourseCodeTXT.resignFirstResponder()
       // passwordText.resignFirstResponder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func createToolbar() {
        
        
        toolBar.sizeToFit()
        
        //Customizations
        toolBar.barTintColor? = .green
        toolBar.tintColor = .black
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(StudentRecordViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        courseTypeSelector.inputAccessoryView = toolBar
        collageSelector.inputAccessoryView = toolBar
        departmentSelector.inputAccessoryView = toolBar
    }
    
    
    func dismissKeyboard() {
        toolBar.isHidden = true

        view.endEditing(true)
        
    }
    
    func createCoursePicker() {
        
        let coursePicker = UIPickerView()
        coursePicker.delegate = self
        coursePicker.tag = 0
        
        courseTypeSelector.inputView = coursePicker
        
        //Customizations
        coursePicker.backgroundColor = .white
    }
    
    
    func createCollagePicker() {
        
        let collagePicker = UIPickerView()
        collagePicker.delegate = self
        collagePicker.tag = 1
        
        collageSelector.inputView = collagePicker
        
        //Customizations
        collagePicker.backgroundColor = .white
    }
    
    
    func createDepertmentPicker() {
        
        let departmentPicker = UIPickerView()
        departmentPicker.delegate = self
        departmentPicker.tag = 2
        
        departmentSelector.inputView = departmentPicker
        
        //Customizations
        departmentPicker.backgroundColor = .white
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        toolBar.isHidden = false
        
        if(pickerView.tag == 2){
            if(selectedCollage == nil){
                dismissKeyboard()
                self.view.makeToast(NSLocalizedString("Please Enter collage", comment: ""), duration: 3.0, position: CSToastPositionCenter)
                return 0
            }
            else{
                return 1

            }
            
        }
        else{
            return 1
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 0){
            return SearchCourseCatalogViewModel.instance.courseType.count
        }
        else if(pickerView.tag == 1){
            return SearchCourseCatalogViewModel.instance.catalogFaculties.count
        }
        else{
            return SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected].department.count
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if(pickerView.tag == 0){
            courseTypeSelector.text = SearchCourseCatalogViewModel.instance.courseType[row].dESCR_EN
            courseTypeSelected = SearchCourseCatalogViewModel.instance.courseType[row].eD_CODE_COURSE_ID!
            
        }
        else if(pickerView.tag == 1){
            collageSelector.text = SearchCourseCatalogViewModel.instance.catalogFaculties[row].nameEn
//            departmentSelector.text = SearchCourseCatalogViewModel.instance.catalogFaculties[0].department[row].nameEn
            depcollageSelected = row
            selectedCollage = SearchCourseCatalogViewModel.instance.catalogFaculties[row].asFacultyInfoId
            departmentSelector.text = SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected].department[0].nameEn
            departmentSelected = SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected].department[0].id!
                //SearchCourseCatalogViewModel.instance.catalogFaculties[row].asFacultyInfoId
        }
        else{
            let x = SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected]
            departmentSelector.text = x.department[row].nameEn
            departmentSelected = SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected].department[row].id!
         //   selectedFaculty = SearchAcademicCalenderViewModel.instance.faculty[row].asFacultyInfoId!
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = .black
        label.textAlignment = .center
        //label.font = UIFont(name: "Menlo-Regular", size: 17)
        if(pickerView.tag == 0){
            label.text = SearchCourseCatalogViewModel.instance.courseType[row].dESCR_EN
            
        }
        else if(pickerView.tag == 1){
            label.text = SearchCourseCatalogViewModel.instance.catalogFaculties[row].nameEn
        }
        else{
            let x = SearchCourseCatalogViewModel.instance.catalogFaculties[depcollageSelected]
            label.text = x.department[row].nameEn
        }
        
        return label
    }
    
    
    
    
    
    
    func update(){
        
    }
    func updateRefresh(){
        
    }
    func update(APiName:String){
        self.stopAnimating()

        if(APiName == url.courseCatalog){
            if(CourseCatalogViewModel.instance.result == "ERROR"){
                self.view.makeToast(NSLocalizedString("No Data", comment: ""), duration: 3.0, position: CSToastPositionCenter)
                
                // navigationController?.popViewController(animated: true)
                
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "CourseCatalog") as! CourseCatalogViewController
                navigationController?.pushViewController(vc, animated: true)
                
            }
        }

    }

    @IBAction func SearchCatalogBTN(_ sender: Any) {
        self.startAnimating(self.size, message: "Loading...", type: .lineScalePulseOutRapid)

        CourseCatalogViewModel.instance.studentRecord(facultyID:selectedCollage! , courseCode: CourseCodeTXT.text!, courseName: "", courseID:courseTypeSelected , departmentID: departmentSelected, degreeClass: 0, onlineFlag: -1, activeFlag: -1, updatable: self)
            }
   

}
