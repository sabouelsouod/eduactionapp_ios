//
//  LanguageViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func arabicBTN(_ sender: Any) {
        var transition: UIViewAnimationOptions = .transitionFlipFromLeft
        
        L102Language.setAppleLAnguageTo(lang: "ar")
        transition = .transitionFlipFromLeft
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "collection")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
//        present(vc,animated: true)

        
    }
    
    @IBAction func englishBTN(_ sender: Any) {
        
        var transition: UIViewAnimationOptions = .transitionFlipFromLeft
        
        L102Language.setAppleLAnguageTo(lang: "en")
        transition = .transitionFlipFromRight
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        rootviewcontroller.rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "collection")
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainwindow, duration: 0.55001, options: transition, animations: { () -> Void in
        }) { (finished) -> Void in
            
        }
       
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
