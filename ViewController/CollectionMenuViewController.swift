//
//  CollectionMenuViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit

class CollectionMenuViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,Updatable{
    
    @IBOutlet weak var menuBTN: UIBarButtonItem!
    @IBOutlet weak var collectionMenu: UICollectionView!
    let collectionPicString = ["accountPic","studentDataPic","studentCalender","settingImage","studentDataPic","studentCalender"]
    let colectionName = ["Student Data","School Record","Courses Registration","Settings","Academic Calender","CourseCatalog"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if L102Language.currentAppleLanguage() == "en" {
            menuBTN.target = self.revealViewController()
            self.revealViewController().rearViewRevealWidth = 300
            menuBTN.action = #selector(SWRevealViewController.revealToggle(_:))
            revealViewController().rearViewController = storyboard?.instantiateViewController(withIdentifier: "RearTableViewController") as? SideMenuViewController
        }
        else{
            
            menuBTN.target = self.revealViewController()
            self.revealViewController().rightViewRevealWidth = 350
            menuBTN.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            revealViewController().rightViewController = storyboard?.instantiateViewController(withIdentifier: "RearTableViewController") as? SideMenuViewController
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        if(UIDevice.current.userInterfaceIdiom == .pad){
            let itemSize = UIScreen.main.bounds.width/3 - 10
            let layout = UICollectionViewFlowLayout	()
            layout.sectionInset = UIEdgeInsetsMake(5, 0, 0, 3)
            layout.itemSize = CGSize(width: itemSize-5, height: itemSize)
            layout.minimumInteritemSpacing = 3
            layout.minimumLineSpacing = 3
            collectionMenu.collectionViewLayout = layout
            
        }
        else{
            let itemSize = UIScreen.main.bounds.width/2-10
            let layout = UICollectionViewFlowLayout	()
            layout.sectionInset = UIEdgeInsetsMake(5, 0, 0, 3)
            layout.itemSize = CGSize(width: itemSize-5, height: itemSize)
            layout.minimumInteritemSpacing = 3
            layout.minimumLineSpacing = 3
            collectionMenu.collectionViewLayout = layout
        }
        /////////                     /////////////////
        let userlogged = UserDefaults.standard.value(forKey: "RememberMeState") as? String ?? ""
        if(userlogged == "On"){
            let name = UserDefaults.standard.value(forKey: "LastUserName") as? String ?? ""
            let password = UserDefaults.standard.value(forKey: "LastPassword") as? String ?? ""
            LoginViewModel.instance.login(username: name, password: password, updatable: self as Updatable)

        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionPicString.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionMenu.dequeueReusableCell(withReuseIdentifier: "collectioncell", for: indexPath) as! CollectionCell
        //cell.collectionName.text = colectionName[indexPath.row]
        cell.collectionImage.image = UIImage(named: collectionPicString[indexPath.row])
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 10;
        cell.collectionName.text = NSLocalizedString(colectionName[indexPath.row], comment: "")
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userlogged2 = UserDefaults.standard.value(forKey: "RememberMeState") as? String ?? ""
        
        if indexPath.row == 0 {
            if userlogged2 == "On"||LoginViewModel.instance.log == 1{
                self.view.makeToast(NSLocalizedString("Login Sucess", comment: ""), duration: 3.0, position: CSToastPositionCenter)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "userData") as! UserDataViewController
                navigationController?.pushViewController(vc,animated: true)
                
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                navigationController?.pushViewController(vc,animated: true)
            }
            
        }
            else if indexPath.row == 1 {
             if userlogged2 == "On"||LoginViewModel.instance.log == 1{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "studentrecord") as! StudentRecordViewController
                navigationController?.pushViewController(vc,animated: true)
            }
             else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                navigationController?.pushViewController(vc,animated: true)
                }
            }
        else if indexPath.row == 2{
            if userlogged2 == "On"||LoginViewModel.instance.log == 1{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "courseregister") as! CourseRegisterationViewController
                navigationController?.pushViewController(vc,animated: true)
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginViewController
                navigationController?.pushViewController(vc,animated: true)
            }
        
        }
        

        else if indexPath.row == 4{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "searchacademic") as! SearchAcademicCalenderViewController
            navigationController?.pushViewController(vc,animated: true)
            
            

    }
        else if indexPath.row == 5{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "searchCatalog") as! SearchCourseCatalogViewController
            navigationController?.pushViewController(vc,animated: true)
            
            
            
        }
    }
    
    
    
    
    
    func update(){
        self.view.makeToast(NSLocalizedString("Login Sucess", comment: ""), duration: 3.0, position: CSToastPositionCenter)
    }
    func updateRefresh(){
        
    }
    func update(APiName:String){
        
    }



    

}
