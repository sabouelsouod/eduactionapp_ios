//
//  AcadmicCalenderViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/21/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AcadmicCalenderViewController: UIViewController,Updatable,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var academicTableView: UITableView!
    var selectedYear:String = ""
    var selectedSemester:String = ""
    var selectedFaculty:Int = 0
    var selectedGrade:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func update(){
        if(AcademicCalenderViewModel.instance.result == "ERROR"){
            self.view.makeToast(NSLocalizedString("No Data", comment: ""), duration: 3.0, position: CSToastPositionCenter)
            
            navigationController?.popViewController(animated: true)

            
        }
        academicTableView.reloadData()
    }
    func updateRefresh(){
        
    }
    func update(APiName:String){
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let academicData = AcademicCalenderViewModel.instance.data
                return academicData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = academicTableView.dequeueReusableCell(withIdentifier: "academiccell", for: indexPath) as! AcademicCalenderTableViewCell
        
        
        let academicData = AcademicCalenderViewModel.instance.data
        if L102Language.currentAppleLanguage() == "en" {
            cell.collageName.text = academicData[indexPath.row].eNT_DESCR_EN
            cell.semesterName.text = academicData[indexPath.row].sEM_EN
            
        }
        else{
            cell.collageName.text = academicData[indexPath.row].eNT_DESCR_AR
            cell.semesterName.text = academicData[indexPath.row].sEM_AR
            
        }

//        cell.collageName.text = academicData[indexPath.row].eNT_DESCR_EN
//        cell.semesterName.text = academicData[indexPath.row].sEM_EN
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: academicData[indexPath.row].tO_DATE!)
        dateFormatter.dateFormat = "yyyy-MM-dd"///this is what you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let to_Date = dateFormatter.string(from: date!);
        
        cell.toDate.text = to_Date
        
        
        
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        dateFormatter1.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date1 = dateFormatter1.date(from: academicData[indexPath.row].fROM_DATE!)
        
        
        dateFormatter1.dateFormat = "yyyy-MM-dd"///this is what you want to convert format
        dateFormatter1.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let from_Date = dateFormatter1.string(from: date1!);
        cell.fromDate.text = from_Date
        
        
        if L102Language.currentAppleLanguage() == "en" {
            cell.activityName.text = academicData[indexPath.row].aCTV_EN

            
        }
        else{
            cell.activityName.text = academicData[indexPath.row].aCTV_AR
            
        }
        //cell.activityName.text = academicData[indexPath.row].aCTV_EN
        
        return cell
    }


   
}
