//
//  SearchAcademicCalenderViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/20/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class SearchAcademicCalenderViewController: UIViewController,UIPickerViewDelegate, UIPickerViewDataSource,Updatable,BEMCheckBoxDelegate,NVActivityIndicatorViewable  {
    

    
    let size = CGSize(width: 30, height: 30)

    
    @IBOutlet weak var AllGradeCheck: BEMCheckBox!
    @IBOutlet weak var underGradeCheck: BEMCheckBox!
    @IBOutlet weak var postGradeCheck: BEMCheckBox!
    
    @IBOutlet weak var yearPicker: UITextField!
    
    @IBOutlet weak var facultyPicker: UITextField!
    
    @IBOutlet weak var semesterPicker: UITextField!
    let url = Constants()
    var selectedYear:String = ""
    var selectedSemester:String = ""
    var selectedFaculty:Int = 0
    var selectedGrade:Int = 0

    
    
    let toolBar = UIToolbar()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.startAnimating(self.size, message: "Loading...", type: .lineScalePulseOutRapid)

        SearchAcademicCalenderViewModel.instance.searchAcademicData(updatable: self)
        createToolbar()
        createYearPicker()
        createFacultyPicker()
        createSemesterPicker()
        AllGradeCheck.delegate = self
        underGradeCheck.delegate = self
        postGradeCheck.delegate = self
        AllGradeCheck.tag = 0
        underGradeCheck.tag = 1
        postGradeCheck.tag = 2

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    func didTap(_ checkBox: BEMCheckBox) {
        print(checkBox.tag)
        if(checkBox.tag == 1 && underGradeCheck.on == true){
            AllGradeCheck.on = false
            postGradeCheck.on = false
            selectedGrade = 1
        }
        else if (checkBox.tag == 2 && postGradeCheck.on == true){
            AllGradeCheck.on = false
            underGradeCheck.on = false
            selectedGrade = 2
        }
        else if (checkBox.tag == 0 && AllGradeCheck.on == true){
            postGradeCheck.on = false
            underGradeCheck.on = false
            selectedGrade = 0
        }
        
        
        
        
        }
    

    func update(){
        
        
    }
    func update(APiName: String) {
        self.stopAnimating()

        if(APiName == url.AcademicCalender){
            if(AcademicCalenderViewModel.instance.result == "ERROR"){
                self.view.makeToast(NSLocalizedString("No Data", comment: ""), duration: 3.0, position: CSToastPositionCenter)
                
                // navigationController?.popViewController(animated: true)
                
            }
            else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "AcademicCalender") as! AcadmicCalenderViewController
                navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    func updateRefresh(){
        
    }
    func createToolbar() {
        
        
        toolBar.sizeToFit()
        
        //Customizations
        toolBar.barTintColor? = .green
        toolBar.tintColor = .black
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(StudentRecordViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        yearPicker.inputAccessoryView = toolBar
        semesterPicker.inputAccessoryView = toolBar
        facultyPicker.inputAccessoryView = toolBar
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
       
    }
    
    func createYearPicker() {
        
        let yearSelector = UIPickerView()
        yearSelector.delegate = self
        yearSelector.tag = 0
        
        yearPicker.inputView = yearSelector
        
        //Customizations
        yearSelector.backgroundColor = .white
    }
    
    
    func createSemesterPicker() {
        
        let semesterSelector = UIPickerView()
        semesterSelector.delegate = self
        semesterSelector.tag = 1
        
        semesterPicker.inputView = semesterSelector
        
        //Customizations
        semesterSelector.backgroundColor = .white
    }
    
    
    func createFacultyPicker() {
        
        let facultyselector = UIPickerView()
        facultyselector.delegate = self
        facultyselector.tag = 2
        
        facultyPicker.inputView = facultyselector
        
        //Customizations
        facultyselector.backgroundColor = .white
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       // toolBar.isHidden = false
        
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(pickerView.tag == 0){
            if(selectedYear == "" ){
            if L102Language.currentAppleLanguage() == "en" {
                yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[0].aCAD_YEAR_DESCR_EN
                
            }
            else{
                yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[0].aCAD_YEAR_DESCR_AR
            }
            // yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_EN
            selectedYear = SearchAcademicCalenderViewModel.instance.AcademicYear[0].eD_ACAD_YEAR_ID!
            return SearchAcademicCalenderViewModel.instance.AcademicYear.count
            }
            else
            {
                return SearchAcademicCalenderViewModel.instance.AcademicYear.count
            }
        }
        else if(pickerView.tag == 1){
            if(selectedSemester == ""){
            if L102Language.currentAppleLanguage() == "en" {
                semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[0].sEMESTER_DESCR_EN
            }
            else{
                semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[0].sEMESTER_DESCR_AR
            }
            //semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_EN
            selectedSemester = SearchAcademicCalenderViewModel.instance.semester[0].eD_STUD_SEMESTER_ID!
                return SearchAcademicCalenderViewModel.instance.semester.count
            }
            else{
                return SearchAcademicCalenderViewModel.instance.semester.count

            }
        }
        else{
            if(selectedFaculty == 0){
            if L102Language.currentAppleLanguage() == "en" {
                facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[0].nameEn
                
            }
            else{
                facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[0].nameAr
                
            }
            facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[0].nameEn
            selectedFaculty = SearchAcademicCalenderViewModel.instance.faculty[0].asFacultyInfoId!

            return SearchAcademicCalenderViewModel.instance.faculty.count
        }
            else{
                return SearchAcademicCalenderViewModel.instance.faculty.count
            }
    }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if(pickerView.tag == 0){
            if L102Language.currentAppleLanguage() == "en" {
                yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_EN
            }
            else{
                yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_AR
            }
           // yearPicker.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_EN
            selectedYear = SearchAcademicCalenderViewModel.instance.AcademicYear[row].eD_ACAD_YEAR_ID!
            
        }
        else if(pickerView.tag == 1){
            if L102Language.currentAppleLanguage() == "en" {
                semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_EN
            }
            else{
                 semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_AR
            }
            //semesterPicker.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_EN
            selectedSemester = SearchAcademicCalenderViewModel.instance.semester[row].eD_STUD_SEMESTER_ID!
        }
        else{
            if L102Language.currentAppleLanguage() == "en" {
                facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameEn

            }
            else{
                facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameAr

            }
            facultyPicker.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameEn
            selectedFaculty = SearchAcademicCalenderViewModel.instance.faculty[row].asFacultyInfoId!
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = .black
        label.textAlignment = .center
        //label.font = UIFont(name: "Menlo-Regular", size: 17)
        if(pickerView.tag == 0){
            if L102Language.currentAppleLanguage() == "en" {
                label.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_EN
                
            }
            else{
                label.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_AR
                
            }
           // label.text = SearchAcademicCalenderViewModel.instance.AcademicYear[row].aCAD_YEAR_DESCR_EN

        }
        else if(pickerView.tag == 1){
            if L102Language.currentAppleLanguage() == "en" {
                label.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_EN
                
            }
            else{
                label.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_AR
                
            }
            //label.text = SearchAcademicCalenderViewModel.instance.semester[row].sEMESTER_DESCR_EN
        }
        else{
            if L102Language.currentAppleLanguage() == "en" {
                label.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameEn

                
            }
            else{
                label.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameAr

                
            }
           // label.text = SearchAcademicCalenderViewModel.instance.faculty[row].nameEn
        }
        
        return label
    }

    @IBAction func searchAcadmicCalender(_ sender: Any) {
         AcademicCalenderViewModel.instance.studentRecord(facultyID: selectedFaculty, academicYear: selectedYear, degreeClassID: selectedGrade, semesterID: selectedSemester, updatable: self)
       // navigationController?.pushViewController(vc,animated: true)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}
