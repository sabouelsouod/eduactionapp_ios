//
//  SideMenuViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var sideTableView: UITableView!
    
    @IBOutlet  var studentName: UILabel!
    @IBOutlet weak var studentPic: UIImageView!
    let sideNames = ["Student Data","Settings","Logout"]
    let collectionPicString = ["studentDataPic","settingImage","studentDataPic"]

    override func viewDidLoad() {
        super.viewDidLoad()
        let userlogged2 = UserDefaults.standard.value(forKey: "RememberMeState") as? String ?? ""

        if userlogged2 == "On"||LoginViewModel.instance.log == 1{
               studentName.text = LoginViewModel.instance.user?.studentName_EN
        }

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = sideTableView.dequeueReusableCell(withIdentifier: "sidetablecell", for: indexPath) as! SideTableCell
        if(indexPath.row == 2){
            cell.tableName.textColor = UIColor.red
        }
        
        cell.tableName.text = NSLocalizedString(sideNames[indexPath.row], comment: "")
        
        cell.tableImage.image = UIImage(named:collectionPicString[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 2){
            UserDefaults.standard.set("Off", forKey: "RememberMeState")
            UserDefaults.standard.set("", forKey: "LastUserName")
            UserDefaults.standard.set("", forKey: "LastPassword")
            LoginViewModel.instance.log = 0
            studentName.text = "Education APP"
            
        }
        
        else if(indexPath.row == 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "language") as! UINavigationController
            present(vc,animated: true)
        }
    }


}
