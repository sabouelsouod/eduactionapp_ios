//
//  LoginViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/13/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoginViewController: UIViewController,Updatable,NVActivityIndicatorViewable {

    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var remeberSwitch: UISwitch!
    let size = CGSize(width: 30, height: 30)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        self.view.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginBTN(_ sender: Any) {
        self.startAnimating(self.size, message: "Loading...", type: .lineScalePulseOutRapid)

        LoginViewModel.instance.login(username: userNameText.text!, password: passwordText.text!, updatable: self)

        
        
    }
    func dismissKeyboard (_ sender: UITapGestureRecognizer) {
        userNameText.resignFirstResponder()
        passwordText.resignFirstResponder()

    }
    func update(){
        self.stopAnimating()

        print("I am in the view controller")
        if self.remeberSwitch.isOn{
            UserDefaults.standard.set("On", forKey: "RememberMeState")
            UserDefaults.standard.set(self.userNameText.text, forKey: "LastUserName")
            UserDefaults.standard.set(self.passwordText.text, forKey: "LastPassword")
            
            
        }
        else
        {
           LoginViewModel.instance.log = 1
        }
          self.view.makeToast(NSLocalizedString("Login Sucess", comment: ""), duration: 3.0, position: CSToastPositionCenter)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "collection") as! SWRevealViewController
        self.present(secondViewController, animated: true)
    }
    
    
    func updateRefresh(){
        
    }
    func update(APiName:String){
        
    }

    
    
    
    

   
}
