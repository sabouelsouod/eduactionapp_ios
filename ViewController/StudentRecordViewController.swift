 //
//  StudentRecordViewController.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/15/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import UIKit
 import NVActivityIndicatorView


class StudentRecordViewController: UIViewController,Updatable,UIPickerViewDelegate, UIPickerViewDataSource,UITableViewDelegate,UITableViewDataSource,NVActivityIndicatorViewable {
    @IBOutlet weak var yearSelector: UITextField!
    @IBOutlet weak var semesterSelector: UITextField!
    @IBOutlet weak var transcriptTableView: UITableView!
    let size = CGSize(width: 30, height: 30)

    
    
    var selectedYear:Int! = nil
    var selectedSemester:Int! = nil
    var selectedSubject:Int! = nil

    let toolBar = UIToolbar()
    let staticGradeArry = ["Enrollment Status:","GPA:","Sem.TotalCH:","Sem.Point:","CGPA:","Accum.Total CH:","Accum.Point:","Major GPA:"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.startAnimating(self.size, message: "Loading...", type: .lineScalePulseOutRapid)
let studentID = LoginViewModel.instance.userData?.studentID
let facultyID = LoginViewModel.instance.userData?.faculty_ID
let academicID = LoginViewModel.instance.userData?.semesterCode
let semesterCode = LoginViewModel.instance.userData?.academicYear
StudentRecordViewModel.instance.studentRecord(studentID:studentID! , facultyInfold: facultyID!, academicYear: academicID!, semesterCode: semesterCode!, updatable: self)
        createYearPicker()
        createSemesterPicker()
        createToolbar()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func update(){
        self.stopAnimating()
    }
    
    
    func updateRefresh(){
        
    }
    func update(APiName:String){
        
    }

    func createToolbar() {
        
        
        toolBar.sizeToFit()
        
        //Customizations
        toolBar.barTintColor? = .green
        toolBar.tintColor = .black
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(StudentRecordViewController.dismissKeyboard))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        yearSelector.inputAccessoryView = toolBar
        semesterSelector.inputAccessoryView = toolBar
    }
    
    
    func dismissKeyboard() {
        view.endEditing(true)
        toolBar.isHidden = true
        if(selectedSemester == nil){}
        else{
         transcriptTableView.reloadData()
        }
    }
    
    func createSemesterPicker() {
        
        let semsterPicker = UIPickerView()
        semsterPicker.delegate = self
        semsterPicker.tag = 2
        
        semesterSelector.inputView = semsterPicker
        
        //Customizations
        semsterPicker.backgroundColor = .white
    }
    
    func createYearPicker() {
        
        let YearPicker = UIPickerView()
        YearPicker.delegate = self
        
        yearSelector.inputView = YearPicker
        
        //Customizations
        YearPicker.backgroundColor = .white
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        toolBar.isHidden = false

        if(pickerView.tag == 2){
            if(selectedYear == nil){
                dismissKeyboard()
                self.view.makeToast(NSLocalizedString("Please Enter Year", comment: ""), duration: 3.0, position: CSToastPositionCenter)
                return 0
            }
            else{
            return 1
            }
        }else{
            return 1

        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        let yearData = StudentRecordViewModel.instance.record
        if(pickerView.tag == 2){
            if(selectedSemester == nil){

            if L102Language.currentAppleLanguage() == "en" {
                semesterSelector.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[0].SEMESTER_DESCR_EN
            }
            else{
                semesterSelector.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[0].SEMESTER_DESCR_AR
            }
            selectedSemester = 0
                return yearData[selectedYear].Semesters.count
                   //StudentRecordViewModel.instance.record[selectedYear].Semesters.count
            }
        else{
            
                return yearData[selectedYear].Semesters.count
        }
    }
        else{
            if(selectedYear == nil){
            if L102Language.currentAppleLanguage() == "en" {
                yearSelector.text = StudentRecordViewModel.instance.record[0].ACAD_YEAR_DESCR_EN
            }
            else{
                yearSelector.text = StudentRecordViewModel.instance.record[0].ACAD_YEAR_DESCR_AR
            }
            //  yearSelector.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_EN
            selectedYear = 0
            return yearData.count
                //StudentRecordViewModel.instance.record.count

        }
            else{
                return yearData.count
            }
    }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        
        if(pickerView.tag == 2){
            if L102Language.currentAppleLanguage() == "en" {
                semesterSelector.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_EN
            }
                else{
                    semesterSelector.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_AR
            }
            //semesterSelector.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_EN
            selectedSemester = row

        }else{
            if L102Language.currentAppleLanguage() == "en" {
                yearSelector.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_EN
            }
            else{
                yearSelector.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_AR
            }
          //  yearSelector.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_EN
            print(row)
            selectedYear = row

        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        
        if let view = view as? UILabel {
            label = view
        } else {
            label = UILabel()
        }
        
        label.textColor = .black
        label.textAlignment = .center
        //label.font = UIFont(name: "Menlo-Regular", size: 17)
        if(pickerView.tag == 2){
            if L102Language.currentAppleLanguage() == "en" {
                label.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_EN
            }
            else{
                label.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_AR
            }
            //label.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[row].SEMESTER_DESCR_EN

        }else{
            if L102Language.currentAppleLanguage() == "en" {
                label.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_EN
            }
            else{
                label.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_AR
            }
           // label.text = StudentRecordViewModel.instance.record[row].ACAD_YEAR_DESCR_EN

        }
        
        
        return label
    }
   
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(selectedYear == nil){
            transcriptTableView.isHidden = true
            return 0
        }else{
            transcriptTableView.isHidden = false
            if(section == 0){
                return StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts.count

            }
            else{
                return staticGradeArry.count

            }

        }
       // return StudentRecordViewModel.instance.record[0].Semesters[0].Transcripts.count
            //StudentRecordViewModel.instance.transcriptTable()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = transcriptTableView.dequeueReusableCell(withIdentifier: "subjectcell", for: indexPath) as! TranscriptTableCell

        if(indexPath.section == 0){
            if L102Language.currentAppleLanguage() == "en" {
                cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[indexPath.row].cOURSE_DESCR_EN
            }
            else{
                cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[indexPath.row].cOURSE_DESCR_AR
            }
            
        //cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[indexPath.row].cOURSE_DESCR_EN
        cell.subjectCode.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[indexPath.row].cOURSE_CODE
        }
        else{
            if L102Language.currentAppleLanguage() == "en" {
            cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[0].semesterGarde().0[indexPath.row] as? String
        }
        else{
            cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[0].semesterGarde().1[indexPath.row] as? String
            }
             //cell.subjectName.text = StudentRecordViewModel.instance.record[selectedYear].Semesters[selectedSemester].Transcripts[0].semesterGarde().0[indexPath.row] as? String
             cell.subjectCode.text = NSLocalizedString(staticGradeArry[indexPath.row], comment: "")
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedSubject = indexPath.row
        if(indexPath.section == 0){
            let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "subjectview") as! SubjectViewController
            secondViewController.subjectYear = selectedYear
            secondViewController.subjectSemester = selectedSemester
            secondViewController.subjectNO = selectedSubject
            self.present(secondViewController, animated: true)

        }
    }
    
    
    
    
    
    let headersName = ["Subject Name","Semester Grade"]
    
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        if(section == 0){
            return NSLocalizedString(headersName[0], comment: "")

        }
        else{
            return NSLocalizedString(headersName[1], comment: "")
        }
        
        }
    
    
    
    
    
}
    
    
    
    
    
    
    
    
    


