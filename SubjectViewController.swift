//
//  SubjectViewController.swift
//  EducationApp
//
//  Created by eslammohamed on 11/19/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class SubjectViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var subjectYear:Int = 0
    var subjectSemester:Int = 0
    var subjectNO:Int = 0
    
    @IBOutlet weak var dsdsds: UIView!
    let subjectStatic = ["Course Grade","Course Degree","Course CreditHour","Course Point","Enrollment Date"]

    @IBOutlet weak var subjectTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard (_:)))
        //tapGesture.ta
//self.view.addGestureRecognizer(tapGesture)

    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)  {
        var touch: UITouch? = touches.first 
        //location is relative to the current view
        // do something with the touched point
        print(touch?.view?.tag)
        if touch?.view != dsdsds {
dismiss(animated: true, completion: nil)
            
        }
    }
//    func dismissKeyboard (_ sender: UITapGestureRecognizer) {
//        //aTextField.resignFirstResponder()
//        print("Eslam")
//        print(tapGesture.view?.tag)
//        if(tapGesture.view == tapGesture.){
//            print("Kaled")
//
//        }else{
//            print("Amr")
//        }
//        
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subjectStatic.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let x = StudentRecordViewModel.instance.record[subjectYear].Semesters[subjectSemester]
        let cell = subjectTableView.dequeueReusableCell(withIdentifier: "subjecttable", for: indexPath) as! SubjectTableViewCell
        
        cell.subjectDynamic.text = x.Transcripts[subjectNO].subjectGrade().1[indexPath.row] as? String
        cell.subjectStatic.text  = subjectStatic[indexPath.row]
        
        return cell
    }
    func tableView( _ tableView : UITableView,  titleForHeaderInSection section: Int)->String? {
        let x = StudentRecordViewModel.instance.record[subjectYear].Semesters[subjectSemester]
            return x.Transcripts[subjectNO].cOURSE_DESCR_EN
    }
   
    

}
