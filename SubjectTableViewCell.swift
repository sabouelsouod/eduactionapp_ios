//
//  SubjectTableViewCell.swift
//  EducationApp
//
//  Created by eslammohamed on 11/19/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit

class SubjectTableViewCell: UITableViewCell {
    @IBOutlet weak var subjectDynamic: UILabel!

    @IBOutlet weak var subjectStatic: UILabel!
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
