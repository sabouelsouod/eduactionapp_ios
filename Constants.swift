//
//  Constants.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation

class Constants: AnyObject {
    let baseURL = "http://192.168.0.219/api/"
    let loginURL = "students/login"
    let studentSchedule = "students/schedule"
    let studentRecord = "students/transcript"
    let searchAcademicCalender = "Filter/FacultiesAcadYearsSemesters"    /// .GET SERVICES
    let AcademicCalender = "semester/semsterCalnd"
    let searchCourseCatalog = "Faculties/CoursesCatalog/Filter"   /// .GET SERVICES
    let courseCatalog = "Faculties/CoursesCatalog"
    


}
