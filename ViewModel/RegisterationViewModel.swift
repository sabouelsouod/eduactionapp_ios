//
//  RegisterationViewModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/19/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper
class RegistrationViewModel: Mappable {
    var result:String?
    var details :String?
    var record = [CourseRegisterModel]()

    static var  instance = RegistrationViewModel()
    let url = Constants()
    var updatable: Updatable?
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        record <- map["items"]

    }
    func studentRecord(studentID: String, academicYear: String, semesterCode: String,updatable: Updatable) {
        let parameters: Parameters = ["EdStudId":studentID,"EdAcadYearId":academicYear,"EdCodeSemesterId":semesterCode] as [String:Any]
        print(parameters)
        RegistrationViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.studentSchedule )!
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<RegistrationViewModel>) in
            print("Inside login completion handler")
            
            print(response.result)
            
            guard response.result.isSuccess
                
                else {
                    print("Error while trying to login: \(response.result.error)")
                    
                    self.onFailure()
                    
                    return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            RegistrationViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
        print(RegistrationViewModel.instance.result!)
       
                updatable?.update()
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update()
        
        
    }

    
    
}
