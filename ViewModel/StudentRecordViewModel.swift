//
//  StudentScheduleViewModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/14/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper
class StudentRecordViewModel: Mappable {
    var result:String?
    var details :String?
    var record = [YearSemestersModel]()
    static var  instance = StudentRecordViewModel()
    let url = Constants()
    var updatable: Updatable?

    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        record <- map["items"]

    }
    func studentRecord(studentID: String,facultyInfold:String, academicYear: String, semesterCode: String,updatable: Updatable) {
         let parameters: Parameters = ["EdStudId":studentID,"AsFacultyInfoId":facultyInfold,"EdAcadYearId":academicYear,"EdCodeSemesterId":semesterCode] as [String:Any]
        print(parameters)
        StudentRecordViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.studentRecord )!
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<StudentRecordViewModel>) in
            print("Inside login completion handler")
            
           print(response.result)
            
            guard response.result.isSuccess
                
                else {
                print("Error while trying to login: \(response.result.error)")
                
                self.onFailure()
                
                return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            StudentRecordViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    
    
    
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
       print(StudentRecordViewModel.instance.record[0].Semesters[0].Transcripts.count)
     let x = StudentRecordViewModel.instance.record[0].Semesters[0].Transcripts[1].semesterGarde().1[0]
  print(x)
        updatable?.update()
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update()
        
        
    }
    func transcriptTable() ->Int{
        let x:Int?
        x = StudentRecordViewModel.instance.record.count
        if(x == 0){
            return 0
        }
        else{
        return StudentRecordViewModel.instance.record[0].Semesters[0].Transcripts.count
        }
    }


        
    }
