//
//  CourseCatalogViewModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/26/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper

class CourseCatalogViewModel: Mappable {
    
    var result:String?
    var details :String?
     var data = [CourseCatalogModel]()

    
    
    
    
    
    static var  instance = CourseCatalogViewModel()
    let url = Constants()
    var updatable: Updatable?
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        data <- map["items"]
        
        
        
        
    }
    func studentRecord(facultyID: Int, courseCode: String, courseName: String,courseID: Int,departmentID:Int,degreeClass:Int,onlineFlag:Int,activeFlag:Int,updatable: Updatable) {
        let parameters: Parameters = ["AsFacultyInfoId" : facultyID, "CrsCode" : courseCode, "CrsName" :courseName
            , "EdCodeCourseId" : courseID, "DeptId" : departmentID, "AsCodeDegreeClassId" : degreeClass, "OnlineFlg" :onlineFlag,"ActiveFlg" : activeFlag] as [String:Any]
        print(parameters)
        CourseCatalogViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.courseCatalog )!
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<CourseCatalogViewModel>) in
            print("Inside login completion handler")
            
            print(response.result)
            
            guard response.result.isSuccess
                
                else {
                    print("Error while trying to login: \(response.result.error)")
                    
                    self.onFailure()
                    
                    return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            CourseCatalogViewModel.instance = result
            
            
            self.onSuccess()
        })
    }

    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
        print(CourseCatalogViewModel.instance.result!)
        print(CourseCatalogViewModel.instance.details!)
       // print(CourseCatalogViewModel.instance.data[0].fACULTY_DESCR_EN)

      //  print(CourseCatalogViewModel.instance.data[0].cOURSE_DESCR_EN)

        updatable?.update(APiName: url.courseCatalog)
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update(APiName: url.courseCatalog)
        
        
    }
    
    
    
    
    
    
    
    
    
}
