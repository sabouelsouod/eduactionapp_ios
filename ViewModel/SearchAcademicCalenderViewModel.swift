//
//  SearchAcademicCalenderViewModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/20/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import UIKit
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper

class SearchAcademicCalenderViewModel: Mappable {
    
    var result:String?
    var details :String?
    var faculty = [SearchFacultyModel]()
    var semester = [SearchSemesterAcademicModel]()
    var AcademicYear = [SearchAcademicYearModel]()

    
    
    static var  instance = SearchAcademicCalenderViewModel()
    let url = Constants()
    var updatable: Updatable?
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        faculty <- map["Faculties"]
        semester <- map["Semester"]
        AcademicYear <- map["AcadYears"]
 
    }

    func searchAcademicData(updatable: Updatable) {
        SearchAcademicCalenderViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.searchAcademicCalender )!
        print(url)
        Alamofire.request(url, method:.get, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<SearchAcademicCalenderViewModel>) in
            print("Inside login completion handler")
            
            print(response.result)
            
            guard response.result.isSuccess
                
                else {
                    print("Error while trying to login: \(response.result.error)")
                    
                    self.onFailure()
                    
                    return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            SearchAcademicCalenderViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
       

        
        updatable?.update(APiName: self.url.searchAcademicCalender)
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update(APiName: self.url.searchAcademicCalender)
        
        
    }

    
    
    

    

   

}
