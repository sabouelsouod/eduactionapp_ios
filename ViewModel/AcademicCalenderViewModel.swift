//
//  AcademicCalenderViewModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/21/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper

class AcademicCalenderViewModel: Mappable {
    
    var result:String?
    var details :String?
    var data = [AcademicCalenderModel]()
   
    
    
    
    static var  instance = AcademicCalenderViewModel()
    let url = Constants()
    var updatable: Updatable?
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        data <- map["items"]

        
    }
    func studentRecord(facultyID: Int, academicYear: String, degreeClassID: Int,semesterID: String,updatable: Updatable) {
        let parameters: Parameters = ["AsFacultyInfoId" :facultyID , "EdAcadYearId" : academicYear, "AsCodeDegreeClassId" :degreeClassID ,"EdCodeSemesterId":semesterID] as [String:Any]
        print(parameters)
        AcademicCalenderViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.AcademicCalender )!
        print(url)
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<AcademicCalenderViewModel>) in
            print("Inside login completion handler")
            
            print(response.result)
            
            guard response.result.isSuccess
                
                else {
                    print("Error while trying to login: \(response.result.error)")
                    
                    self.onFailure()
                    
                    return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            AcademicCalenderViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
        print(AcademicCalenderViewModel.instance.result!)
        print(AcademicCalenderViewModel.instance.details!)
        
        updatable?.update(APiName: self.url.AcademicCalender)
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update(APiName: self.url.AcademicCalender)
        
        
    }

    
    
    
    
    
}

