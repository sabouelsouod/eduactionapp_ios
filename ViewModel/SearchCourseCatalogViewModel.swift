//
//  SearchCourseCatalogViewModel.swift
//  EducationApp
//
//  Created by eslammohamed on 11/22/17.
//  Copyright © 2017 eslammohamed. All rights reserved.
//


import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper

class SearchCourseCatalogViewModel: Mappable {
    
    var result:String?
    var details :String?
    var courseType = [CourseTypeModel]()
    var catalogFaculties = [CatalogFacultiesModel]()

   
    
    
    
    static var  instance = SearchCourseCatalogViewModel()
    let url = Constants()
    var updatable: Updatable?
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        courseType<-map["CourseTypes"]
        catalogFaculties<-map["Faculties"]


        
        
}
    func searchAcademicData(updatable: Updatable) {
        SearchAcademicCalenderViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.searchCourseCatalog )!
        print(url)
        Alamofire.request(url, method:.get, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<SearchCourseCatalogViewModel>) in
            print("Inside login completion handler")
            
            print(response.result)
            
            guard response.result.isSuccess
                
                else {
                    print("Error while trying to login: \(response.result.error)")
                    
                    self.onFailure()
                    
                    return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            SearchCourseCatalogViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")
        print(SearchCourseCatalogViewModel.instance.result!)
        print(SearchCourseCatalogViewModel.instance.details!)
       
        updatable?.update(APiName: url.searchCourseCatalog)
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update(APiName: url.searchCourseCatalog)
        
        
    }
    
    
    
    
    
    
    
    
    
}

