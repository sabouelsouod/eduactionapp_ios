//
//  LoginViewModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/13/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

//
//  LoginViewModel.swift
//  MVCVMTest
//
//  Created by eslammohamed on 11/13/17.
//  Copyright © 2017 Saad Abou-Elsouod. All rights reserved.
//

import Foundation
import Alamofire
import EVReflection
import ObjectMapper
import AlamofireObjectMapper
class LoginViewModel: Mappable {
    var result:String?
    var details :String?
    var user: UserModel?
    var userData: UserDataModel?
    static var  instance = LoginViewModel()
    let url = Constants()
    var updatable: Updatable?

    var log = 0
    
    
    init() {
        
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        result <- map["result"]
        details <- map["details"]
        user <- map["CardProfile"]
        userData <- map["UserMainData"]
    }
    
    func login(username: String, password: String, updatable: Updatable) {
        
        
        LoginViewModel.instance.updatable = updatable
        
        let url = URL(string: self.url.baseURL + self.url.loginURL )!
        print(url)
        //        let parameters = ["UserName":username,"Password":password,"LanguageID":1] as [String : Any]
        
        var parameters: [String: Any] = [String: Any]()
        parameters["UserName"] = username
        parameters["Password"] = password
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default).validate().responseObject(completionHandler: { (response: DataResponse<LoginViewModel>) in
            
            print("Inside login completion handler")
            print(response.result)
            
            guard response.result.isSuccess else {
                print("Error while trying to login: \(response.result.error)")
                
                self.onFailure()
                
                return
            }
            
            guard let result = response.result.value else {
                
                print("Error in casting result!")
                
                self.onFailure()
                
                return
            }
            
            LoginViewModel.instance = result
            
            
            self.onSuccess()
        })
    }
    
    
    
    func onSuccess() {
        print("SUCCESS")
        print("I am in the view Model")

        updatable?.update()
        
        
    }
    
    func onFailure() {
        print("Failed to call login web service")
        
        updatable?.update()
        
        
    }
}
